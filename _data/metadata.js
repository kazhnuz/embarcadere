module.exports = {
	title: "Kobold Start",
	url: "https://start.kobold.cafe/",
	language: "fr",
	description: "Un petit portail internet tout bête",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
